1.安装Markdown Preview 插件

输入 Shift + Ctrl + P，输入pcip（也可以点击 Preferences --> 选择 Package Control: ，然后输入install），然后在插件库中分别选择安装Markdown Preview

2.自定义快捷键
直接在浏览器中预览效果的话，可以自定义快捷键：点击 Preferences --> 选择 Key Bindings User，输入：{ "keys": ["alt+m"], "command": "markdown_preview", "args": {"target": "browser", "parser":"markdown"} },保存后，直接输入快捷键：Alt + M 就可以直接在浏览器中预览生成的HTML文件了

3.编辑Markdown文件
按Ctrl + N 新建一个文档按Ctrl + Shift + P使用Markdown语法编辑文档语法高亮，输入ssm 后回车(Set Syntax: Markdown)编写完保存以.md为后缀的文件

4.使用 Markdown Preview在浏览器中预览
  * 直接输入快捷键：Alt + M 就可以直接在浏览器中预览生成的HTML文件了
  * 按Ctrl + Shift + P使用浏览器中预览，输入markdown preview后回车(Markdown Preview: preview in browser)，就可以在浏览器里看到刚才编辑的文档了
  
5.使用 Markdown Preview 生成 HTML
按Ctrl + Shift + P使用浏览器中预览，输入markdown preview后，选择save to html 回车，然后选择markdown会在当前目录下生成同名的html文件